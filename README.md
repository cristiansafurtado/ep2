# EP2 - OO 2019.1 (UnB - Gama)


## Funções possíveis ao programa:

 - O programa consegue adicionar de uma vez só mais de um tipo de veículo se necessário.
 - Também é possível ver e remover estes veículos adicionados através da aba "Remover Veiculos".
 - É possível alterar/consultar a porcentagem da margem de lucro, não permitindo ultrapassar 0% ou 100%.


## Deficiências do programa:

 - O programa não consegue criar novos pedidos de fretes, apesar das condições colocadas dentro do programa.
 - Como não consegue criar pedidos de fretes, além da opção de 'Mostrar Fretes' no menu ficar indisponível, não é possível fazer o cálculo do melhor veículo para o frete.
 - O programa não salva os veículos adicionados, ou seja, cada vez que se abre o programa, a frota é resetada.
