/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frete;


import Veiculos.*;
/**
 *
 * @author crist
 */
public class Frete {
    private String nomeCliente;
    private double carga;
    private Veiculo veiculoTransportador;
    private double distanciaViagem;
    private double custo;    
    
    public Frete(String nomeCliente, double carga, Veiculo veiculoTransportador, double distanciaViagem) {
        this.nomeCliente = nomeCliente;
        this.carga = carga;
        this.veiculoTransportador = veiculoTransportador;
        this.distanciaViagem = distanciaViagem;
        defineCustoFrete();
    }
    
    public double getCarga() {
        return carga;
    }

    public void setCarga(double carga) {
        this.carga = carga;
    }

    public Veiculo getVeiculoTransportador() {
        return veiculoTransportador;
    }

    public void setVeiculoTransportador(Veiculo veiculoTransportador) {
        this.veiculoTransportador = veiculoTransportador;
    }

    public double getDistanciaViagem() {
        return distanciaViagem;
    }

    public void setDistanciaViagem(double distanciaViagem) {
        this.distanciaViagem = distanciaViagem;
    }

    public double getCusto() {
        return custo;
    }

    public void setCusto(double custo) {
        this.custo = custo;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
    
    public double defineCustoFrete(){
        return this.custo = veiculoTransportador.getRendimento() * distanciaViagem;
    }
    
//    public double defineTempoViagem(){
//        return this.distanciaViagem = veiculoTransportador.getVelMax() * 
//    }
    
    
    
}
