/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veiculos;

/**
 *
 * @author crist
 */
public class Moto extends Veiculo {
    
    public Moto(){
        setDisponibilidade("Disponivel");
        setTipoVeiculo("Moto");
        setCargaMax(50.00);
        setVelMax(110.0);
        setCombustivel("Gasolina ou Alcool");
    }
    
    public Moto(String combustivel){
        setDisponibilidade("Disponivel");
        setTipoVeiculo("Moto");
        setCargaMax(50.00);
        setVelMax(110.0);
        setCombustivel(combustivel);
        
        if(combustivel == "Gasolina")
            setRendimento(50.0);
        if(combustivel == "Alcool")
            setRendimento(43.0);
    }
        
    @Override
    public void reduzRendimento(double carga){
        if(getCombustivel() == "Gasolina")
            setRendimento(50.0 - carga*(0.3));
        if(getCombustivel() == "Alcool")
            setRendimento(43.0 - carga*(0.4));
    }
    
}
