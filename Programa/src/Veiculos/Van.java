/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veiculos;

/**
 *
 * @author crist
 */
public class Van extends Veiculo {
    
    public Van(){
        setDisponibilidade("Disponivel");
        setTipoVeiculo("Van");
        setCombustivel("Diesel");
        setRendimento(10.0);
        setCargaMax(3500.00);
        setVelMax(80.0);
    }
        @Override
    public void reduzRendimento(double carga){
        setRendimento(10 - carga*(0.001));
    }
}
