/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veiculos;

/**
 *
 * @author crist
 */
public class Carreta extends Veiculo {
    
    public Carreta(){
        setDisponibilidade("Disponivel");
        setTipoVeiculo("Carreta");
        setCombustivel("Diesel");
        setRendimento(8.0);
        setCargaMax(30000.0);
        setVelMax(60.0);
    }
    
    @Override
    public void reduzRendimento(double carga){
        setRendimento(8 - carga*(0.0002));
    }
    
    
}
