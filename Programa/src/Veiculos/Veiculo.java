/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veiculos;

/**
 *
 * @author crist
 */
public abstract class Veiculo {
    private String tipoVeiculo;
    private String disponibilidade;
    private String combustivel;
    private double rendimento;
    private double cargaMax;
    private double velMax;

    public Veiculo() {
        this.disponibilidade = "";
        this.tipoVeiculo = "";
        this.combustivel = "";
        this.rendimento = 0.0;
        this.cargaMax = 0.0;
        this.velMax = 0.0;
    } 
    
    public String getDisponibilidade() {
        return disponibilidade;
    }

    public void setDisponibilidade(String disponibilidade) {
        this.disponibilidade = disponibilidade;
    }   
    
    public String getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(String tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }
    
    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public double getRendimento() {
        return rendimento;
    }

    public void setRendimento(double rendimento) {
        this.rendimento = rendimento;
    }

    public double getCargaMax() {
        return cargaMax;
    }

    public void setCargaMax(double cargaMax) {
        this.cargaMax = cargaMax;
    }

    public double getVelMax() {
        return velMax;
    }

    public void setVelMax(double velMax) {
        this.velMax = velMax;
    }
   
    public abstract void reduzRendimento(double carga);
    
}
