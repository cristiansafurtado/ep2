/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veiculos;

/**
 *
 * @author crist
 */
public class Carro extends Veiculo {
        
    public Carro(){
        setDisponibilidade("Disponivel");
        setTipoVeiculo("Carro");
        setCargaMax(360.00);
        setVelMax(100.0);
        setCombustivel("Gasolina ou Alcool");
    }
    
    public Carro (String combustivel){
        setDisponibilidade("Disponivel");
        setTipoVeiculo("Carro");
        setCargaMax(360.00);
        setVelMax(100.0);
        setCombustivel(combustivel);
        
        if(combustivel == "Gasolina")
            setRendimento(14.0);
        if(combustivel == "Alcool")
            setRendimento(12.0);   
    }
    
    @Override
    public void reduzRendimento(double carga){
        if(getCombustivel() == "Gasolina")
            setRendimento(14.0 - carga*(0.025));
        if(getCombustivel() == "Alcool")
            setRendimento(12.0 - carga*(0.0231));
    }
}
