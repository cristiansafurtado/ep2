/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceGrafica;

import java.awt.Dimension;
import java.awt.Toolkit;
import Frete.*;
import Veiculos.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author crist
 */
public class TelaInicial extends javax.swing.JFrame {

    /**
     * Creates new form TelaInicial
     */
    public TelaInicial() {
        margemLucro = 0.0f;
        frota = new ArrayList<Veiculo>();
        fretesEmAndamento = new ArrayList<Frete>();
        initComponents();
        centralizarComponente();
    }

    public void centralizarComponente() { 
        Dimension ds = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension dw = getSize(); 
        setLocation((ds.width - dw.width) / 2, (ds.height - dw.height) / 2); 
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        BarraOpcoesMenu = new javax.swing.JMenuBar();
        MenuVeiculos = new javax.swing.JMenu();
        OpcaoAbreJanelaAdicionarVeiculos = new javax.swing.JMenuItem();
        OpcaoAbreJanelaRemoverVeiculos = new javax.swing.JMenuItem();
        MenuMargemLucro = new javax.swing.JMenu();
        MenuNovoFrete = new javax.swing.JMenuItem();
        MenuMostrarFretes = new javax.swing.JMenuItem();
        MenuAtualizarMargemLucro = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Gerenciador de Frotas");

        jDesktopPane1.setToolTipText("");

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 560, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 361, Short.MAX_VALUE)
        );

        MenuVeiculos.setText("Veiculos");

        OpcaoAbreJanelaAdicionarVeiculos.setText("Adicionar Veiculos");
        OpcaoAbreJanelaAdicionarVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpcaoAbreJanelaAdicionarVeiculosActionPerformed(evt);
            }
        });
        MenuVeiculos.add(OpcaoAbreJanelaAdicionarVeiculos);

        OpcaoAbreJanelaRemoverVeiculos.setText("Remover Veiculos");
        OpcaoAbreJanelaRemoverVeiculos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpcaoAbreJanelaRemoverVeiculosActionPerformed(evt);
            }
        });
        MenuVeiculos.add(OpcaoAbreJanelaRemoverVeiculos);

        BarraOpcoesMenu.add(MenuVeiculos);

        MenuMargemLucro.setText("Frete");

        MenuNovoFrete.setText("Novo Frete");
        MenuNovoFrete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuNovoFreteActionPerformed(evt);
            }
        });
        MenuMargemLucro.add(MenuNovoFrete);

        MenuMostrarFretes.setText("Mostrar Fretes");
        MenuMostrarFretes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuMostrarFretesActionPerformed(evt);
            }
        });
        MenuMargemLucro.add(MenuMostrarFretes);

        MenuAtualizarMargemLucro.setText("Atualizar Margem de Lucro");
        MenuAtualizarMargemLucro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuAtualizarMargemLucroActionPerformed(evt);
            }
        });
        MenuMargemLucro.add(MenuAtualizarMargemLucro);

        BarraOpcoesMenu.add(MenuMargemLucro);

        setJMenuBar(BarraOpcoesMenu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void OpcaoAbreJanelaAdicionarVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpcaoAbreJanelaAdicionarVeiculosActionPerformed
        // Abre janela de adição de mais veículos:
        JanelaInternaAdicionarVeiculos janelaAdicionarVeiculos = new JanelaInternaAdicionarVeiculos();
        jDesktopPane1.add(janelaAdicionarVeiculos);
        janelaAdicionarVeiculos.setVisible(true);
        
        
    }//GEN-LAST:event_OpcaoAbreJanelaAdicionarVeiculosActionPerformed

    private void OpcaoAbreJanelaRemoverVeiculosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpcaoAbreJanelaRemoverVeiculosActionPerformed
        // Abre janela para remoção de veiculos:
        JanelaInternaRemoverVeiculo janelaRemoverVeiculos = new JanelaInternaRemoverVeiculo();
        jDesktopPane1.add(janelaRemoverVeiculos);
        janelaRemoverVeiculos.setVisible(true);     
        
    }//GEN-LAST:event_OpcaoAbreJanelaRemoverVeiculosActionPerformed

    private void MenuNovoFreteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuNovoFreteActionPerformed
        // Abre janela para fazer um pedido de frete:
        if(!frota.isEmpty()){
            JanelaInternaNovoFrete janelaNovoFrete = new JanelaInternaNovoFrete();
            jDesktopPane1.add(janelaNovoFrete);
            janelaNovoFrete.setVisible(true);
        }
        else{
            JOptionPane.showMessageDialog(null, "Não há veículos na frota!\nAdicione alguns primeiro antes de realizar o pedido de frete!");
        }
    }//GEN-LAST:event_MenuNovoFreteActionPerformed

    private void MenuAtualizarMargemLucroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuAtualizarMargemLucroActionPerformed
        // Abre janela com opções para mudar a margem de frete:
        JanelaInternaModificarMargemLucro janelaMargemLucro = new JanelaInternaModificarMargemLucro();
        jDesktopPane1.add(janelaMargemLucro);
        janelaMargemLucro.setVisible(true);
        
    }//GEN-LAST:event_MenuAtualizarMargemLucroActionPerformed

    private void MenuMostrarFretesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuMostrarFretesActionPerformed
        // Abre janela com os fretes em andamento:
        if(!fretesEmAndamento.isEmpty()){
            JanelaInternaMostraFretes janelaMostraFretes = new JanelaInternaMostraFretes();
            jDesktopPane1.add(janelaMostraFretes);
            janelaMostraFretes.setVisible(true);
        }
        else{
            JOptionPane.showMessageDialog(null, "Não há pedidos de frete ainda.");
        }
    }//GEN-LAST:event_MenuMostrarFretesActionPerformed
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaInicial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaInicial().setVisible(true);
            }
        });
    }
    

    public static List<Veiculo> getFrota() {
        return frota;
    }

    public static void setFrota(List<Veiculo> frota) {
        TelaInicial.frota = frota;
    }

    public static float getMargemLucro() {
        return margemLucro;
    }

    public static void setMargemLucro(float margemLucro) {
        TelaInicial.margemLucro = margemLucro;
    }

    public static List<Frete> getFretesEmAndamento() {
        return fretesEmAndamento;
    }

    public static void setFretesEmAndamento(List<Frete> fretesEmAndamento) {
        TelaInicial.fretesEmAndamento = fretesEmAndamento;
    }
    
    private static float margemLucro; 
    private static List<Frete> fretesEmAndamento;
    private static List<Veiculo> frota;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar BarraOpcoesMenu;
    private javax.swing.JMenuItem MenuAtualizarMargemLucro;
    private javax.swing.JMenu MenuMargemLucro;
    private javax.swing.JMenuItem MenuMostrarFretes;
    private javax.swing.JMenuItem MenuNovoFrete;
    private javax.swing.JMenu MenuVeiculos;
    private javax.swing.JMenuItem OpcaoAbreJanelaAdicionarVeiculos;
    private javax.swing.JMenuItem OpcaoAbreJanelaRemoverVeiculos;
    private javax.swing.JDesktopPane jDesktopPane1;
    // End of variables declaration//GEN-END:variables
}
